import bs4
import requests
from bs4 import BeautifulSoup

def parsePrice():
    r=requests.get("https://finance.yahoo.com/quote/FB?p+FB")
    soup=bs4.BeautifulSoup(r.text, "xml")
    price =soup.find_all('div',{'class':"My(6px) Pos(r) smartphone_Mt(6px)"})[0].find('span').text
    return price

#soup.find_all('div',{'class':"My(6px) Pos(r) smartphone_Mt(6px)"})

if True:
    print("The current price:", str(parsePrice()))

def parsePrice1():
    #Below requests for the website
    r=requests.get("https://finance.yahoo.com/quote/%5EGSPC?p=^GSPC")
    #soup parses out the website similar to lxml
    soup=bs4.BeautifulSoup(r.text, "xml")
    #this provides a specific location of the item wanted
    #right click the element and click inspect
    #this locates the information you want
    #soup.findalll will parse through the soup and look
    #for the specific text within the function called
    #div is first called since html starts with div
    #, to seperate the string
    #{ curly brace to check only paremeter within the curly brace
    #class is where the information is called
    #: colon to look for a specific portion
    #} to end the curly brace search
    #) close parentheses to end the search within that area
    #[0] this will list the first finding of an object
    #.find to specify the an area
    #'span' to look within the span since the div and class were searched
    #.text since that is the object type that we want to find
    price =soup.find_all('div',{'class':"My(6px) Pos(r) smartphone_Mt(6px)"})[0].find('span').text
    #returns the price variable as an output of this function
    return price

if True:
    print("The current price of S&P 500 is :", str(parsePrice1()))

# def parsePrice2():
#     #Below requests for the website
#     #zillow has a captcha so the website cannot be webscaped easily
#     r=requests.get("https://www.zillow.com/homes/for_rent/San-Diego-CA-92126/96667_rid/32.970004,-117.110567,32.843611,-117.179747_rect/12_zm/")
#     #soup parses out the website similar to lxml
#     soup=bs4.BeautifulSoup(r.text, "xml")
#     #this provides a specific location of the item wanted
#     #right click the element and click inspect
#     #this locates the information you want
#     #soup.findalll will parse through the soup and look
#     #for the specific text within the function called
#     #div is first called since html starts with div
#     #, to seperate the string
#     #{ curly brace to check only paremeter within the curly brace
#     #class is where the information is called
#     #: colon to look for a specific portion
#     #} to end the curly brace search
#     #) close parentheses to end the search within that area
#     #[0] this will list the first finding of an object
#     #.find to specify the an area
#     #'span' to look within the span since the div and class were searched
#     #.text since that is the object type that we want to find
#     price =soup.find_all('div',{'class':"zsg-photo-card-caption"}).find('span').text
#     #returns the price variable as an output of this function
#     return price

# if True:
#     print("The current price of SD apt is :", str(parsePrice2()))