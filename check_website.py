from lxml import html
import requests


#requests.get to retrieve the webpage
#from lxml, html will parse the html data from requests.get

#make sure to export the website that you want to web scrape
#do not copy and paste from a training website
page = requests.get("http://econpy.pythonanywhere.com/ex/001.html")
tree = html.fromstring(page.content)

#page.content is used rather than page.text because
#page.content provide the data in bytes which
#html can properly parse

#tree contains HTML file in a tree structure that parses
#the information in XPath and CSSSelect

#Xpath stands for XML Path Language
#Xpath syntax uses a "path like" terms i.e. 
#identify and navigate nodes in an XML document
#Think of a traditional file system that has branching paths

#econpy.pythonanywhere.com is read through the Xpath query
#<div title="buyer-name">Carson Busses</div>
#<span class="item-price">$29.95</span>

#list of buyers:
buyers = tree.xpath('//div[@title="buyer-name"]/text()')
#list of prices
prices = tree.xpath('//span[@class="item-price"]/text()')



print("Buyers: ", 
       buyers)
print("Prices: ", 
       prices)
